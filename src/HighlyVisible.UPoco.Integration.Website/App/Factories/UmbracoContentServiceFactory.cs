﻿namespace HighlyVisible.UPoco.Integration.Website.App.Factories
{
	using Mapper.Models;
	using Repositories;
	using Services;

	public class UmbracoContentServiceFactory : IUmbracoContentServiceFactory
	{
		public IUmbracoContentService<T> Get<T>() where T : UmbracoEntity, new()
		{
			return new UmbracoContentService<T>(new UmbracoContentRepository<T>());
		}
	}
}