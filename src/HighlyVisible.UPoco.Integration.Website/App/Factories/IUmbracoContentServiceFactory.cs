﻿namespace HighlyVisible.UPoco.Integration.Website.App.Factories
{
	using Mapper.Models;
	using Services;

	public interface IUmbracoContentServiceFactory
	{
		IUmbracoContentService<T> Get<T>() where T : UmbracoEntity, new();
	}
}