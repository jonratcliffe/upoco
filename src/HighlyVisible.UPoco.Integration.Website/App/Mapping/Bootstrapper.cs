﻿namespace HighlyVisible.UPoco.Integration.Website.App.Mapping
{
	using System.Collections.Generic;
	using System.Linq;
	using Mapper;
	using Modules;

	public class Bootstrapper : IBootstrapper
	{
		private readonly IEnumerable<IModule> _modules;

		public Bootstrapper(IEnumerable<IModule> modules)
		{
			_modules = modules;
		}

		public void Init()
		{
			foreach (var module in _modules.OrderBy(x => x.Priority))
			{
				module.Init();
			}

			Mapper.CachingEnabled = false;

		}
	}
}