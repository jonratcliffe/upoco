﻿namespace HighlyVisible.UPoco.Integration.Website.App.Mapping
{
	public interface IBootstrapper
	{
		void Init();
	}
}