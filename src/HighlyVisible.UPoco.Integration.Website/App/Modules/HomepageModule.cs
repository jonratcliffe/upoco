﻿namespace HighlyVisible.UPoco.Integration.Website.App.Modules
{
	using Mapper;
	using Models;

	public class HomepageModule : IModule
	{

		public int Priority { get { return 1; } }

		public void Init()
		{
			Mapper.CreateMap<Homepage>();
		}
	}
}