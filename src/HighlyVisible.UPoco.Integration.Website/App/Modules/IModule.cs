﻿namespace HighlyVisible.UPoco.Integration.Website.App.Modules
{
	public interface IModule
	{
		int Priority { get; }
		void Init();
	}
}