﻿namespace HighlyVisible.UPoco.Integration.Website.App.Contexts
{
	using System;
	using System.Web;
	using Mapper;
	using Umbraco.Core.Models;

	[Serializable]
	public class CmsPreviewContext
	{
		private static readonly object _syncLock = new object();
		private static CmsPreviewContext _instance = new CmsPreviewContext();
		public bool IsInPreviewMode { get; set; }
		public int OriginatingContentId { get; set; }


		public bool IsInBackOfficeMode
		{
			set
			{
				if (value)
				{
					IsInPreviewMode = true;
					OriginatingContentId = -1;
				}
				else
				{
					IsInPreviewMode = false;
					OriginatingContentId = 0;
				}
			}
		}

		public PreviewState PreviewState
		{
			get
			{
				return new PreviewState
				{
					IsInPreviewMode = IsInPreviewMode,
					OriginatingContentId = OriginatingContentId
				};
			}
		}

		public static CmsPreviewContext Current
		{
			get
			{
				if (HttpContext.Current == null || HttpContext.Current.Session == null)
				{
					return _instance;
				}

				if (HttpContext.Current.Session["CmsPreviewContext"] == null)
				{
					lock (_syncLock)
					{
						if (HttpContext.Current.Session["CmsPreviewContext"] == null)
						{
							HttpContext.Current.Session["CmsPreviewContext"] = new CmsPreviewContext();
						}
					}
				}

				return HttpContext.Current.Session["CmsPreviewContext"] as CmsPreviewContext;
			}
		}

		public bool ShouldShowPublishedVersion(IContent content)
		{
			var shouldShowPublishedVersion = true;

			if (IsInPreviewMode)
			{
				if (OriginatingContentId == content.Id)
				{
					shouldShowPublishedVersion = false;
				}
			}

			return shouldShowPublishedVersion;
		}
	}
}