﻿namespace HighlyVisible.UPoco.Integration.Website.App.Repositories
{
	public interface IUmbracoContentRepository<out T>
	{
		T Get(int nodeId);
	}
}