﻿namespace HighlyVisible.UPoco.Integration.Website.App.Repositories
{
	using Contexts;
	using Mapper;
	using Mapper.Models;

	public class UmbracoContentRepository<T> : IUmbracoContentRepository<T> where T : UmbracoEntity, new()
	{
		public T Get(int nodeId)
		{
			return Mapper.Query<T>().Find(nodeId, CmsPreviewContext.Current.PreviewState);
		}
	}
}