﻿namespace HighlyVisible.UPoco.Integration.Website.App
{
	using System.Web.Mvc;
	using Autofac;
	using Mapping;
	using Umbraco.Core;

	public class UmbracoApplicationHandlers : ApplicationEventHandler
	{
		public static ApplicationContext ApplicationContext { get; private set; }

		protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		{
			AreaRegistration.RegisterAllAreas();

			//WebApiConfig.Register(GlobalConfiguration.Configuration);
			//FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			//RouteConfig.RegisterRoutes(RouteTable.Routes);

			//DefaultRenderMvcControllerResolver.Current.SetDefaultControllerType(typeof(HomepageController));
			base.ApplicationStarting(umbracoApplication, applicationContext);
		}

		protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		{
			ApplicationContext = ApplicationContext;
			AutofacConfig.RegisterContainer();
			AutofacConfig.Container.Resolve<IBootstrapper>().Init();

		}
	}
}

