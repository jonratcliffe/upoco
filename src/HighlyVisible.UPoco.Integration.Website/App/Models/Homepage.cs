﻿namespace HighlyVisible.UPoco.Integration.Website.App.Models
{
	using Mapper.Models;

	public class Homepage : UmbracoEntity
	{
		public string Title { get; set; }
	}
}