﻿namespace HighlyVisible.UPoco.Integration.Website.App.DependencyResolution
{
	using Autofac;
	using Factories;
	using Mapping;
	using Modules;
	using Repositories;
	using Services;
	using Umbraco.Web;

	public class DefaultModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<Bootstrapper>().As<IBootstrapper>().SingleInstance();
			builder.RegisterType<HomepageModule>().As<IModule>().SingleInstance();

			builder.RegisterGeneric(typeof (UmbracoContentRepository<>)).As(typeof (IUmbracoContentRepository<>)).SingleInstance();
			builder.RegisterGeneric(typeof(UmbracoContentService<>)).As(typeof(IUmbracoContentService<>)).SingleInstance();

			builder.RegisterType<UmbracoContentServiceFactory>()
				.As<IUmbracoContentServiceFactory>()
				.SingleInstance()
				.PropertiesAutowired();

			builder.RegisterInstance(UmbracoContext.Current).ExternallyOwned();
		}
	}
}
