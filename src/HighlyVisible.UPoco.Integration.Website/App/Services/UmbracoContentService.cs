﻿namespace HighlyVisible.UPoco.Integration.Website.App.Services
{
	using Mapper.Models;
	using Repositories;

	public class UmbracoContentService<T> : IUmbracoContentService<T> where T : UmbracoEntity, new()
	{
		private readonly IUmbracoContentRepository<T> _umbracoContentRepository;

		public UmbracoContentService(IUmbracoContentRepository<T> umbracoContentRepository)
		{
			_umbracoContentRepository = umbracoContentRepository;
		}

		public T Get(int id)
		{
			return _umbracoContentRepository.Get(id);
		}
	}
}