﻿namespace HighlyVisible.UPoco.Integration.Website.App.Services
{
	using Mapper.Models;

	public interface IUmbracoContentService<out T> where T : UmbracoEntity, new()
	{
		T Get(int id);
	}
}