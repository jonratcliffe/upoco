﻿namespace HighlyVisible.UPoco.Integration.Website
{
	using System.Reflection;
	using System.Web.Mvc;
	using Autofac;
	using Autofac.Integration.Mvc;
	using App.DependencyResolution;

	public class AutofacConfig
	{
		public static IContainer Container { get; private set; }

		public static void RegisterContainer()
		{
			var builder = new ContainerBuilder();
			builder.RegisterModule(new DefaultModule());
			builder.RegisterSource(new ViewRegistrationSource());
			builder.RegisterControllers(Assembly.GetExecutingAssembly());
			builder.RegisterFilterProvider();


			Container = builder.Build();

			DependencyResolver.SetResolver(new AutofacDependencyResolver(Container));

		}
	}
}