﻿using System.Web.Mvc;

namespace HighlyVisible.UPoco.Integration.Website.Controllers
{
	using App.Factories;
	using App.Models;
	using Autofac;
	using Umbraco.Web.Models;
	using Umbraco.Web.Mvc;

	public class HomepageController : RenderMvcController
	{


		public IUmbracoContentServiceFactory ServiceFactory
		{
			get { return AutofacConfig.Container.Resolve<IUmbracoContentServiceFactory>(); }
		}


		public ActionResult Index(RenderModel model)
		{
			return CurrentTemplate(ServiceFactory.Get<Homepage>().Get(CurrentPage.Id));
		}

	}
}
