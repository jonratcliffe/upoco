﻿using System;

namespace HighlyVisible.UPoco.Mapper.Exceptions
{
    /// <summary>
    /// The node type alias was not found in the current Umbraco instance.
    /// </summary>
    public class ContentTypeNotFoundException : Exception
    {
        /// <param name="contentTypeAlias">The node type alias requested and not found</param>
        public ContentTypeNotFoundException(string contentTypeAlias)
            : base(string.Format(@"The content type with alias '{0}' could not be found.  
Consider using the overload of CreateMap which specifies a content type alias", contentTypeAlias))
        {
        }
    }
}