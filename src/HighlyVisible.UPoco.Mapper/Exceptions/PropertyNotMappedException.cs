﻿using System;

namespace HighlyVisible.UPoco.Mapper.Exceptions
{
    /// <summary>
    /// A property was not mapped, but was assumed to be.
    /// </summary>
    public class PropertyNotMappedException : Exception
    {
        /// <param name="type">The model type being mapped to</param>
        /// <param name="propertyName">The name of the property which is not mapped.</param>
        public PropertyNotMappedException(Type type, string propertyName)
            :base(string.Format(@"The property '{0}' on type '{1}' does not have a mapping.
Use the mapping expression returned by INodeMappingEngine.CreateMap() to ensure one exists.", propertyName, type))
        {
        }
    }
}