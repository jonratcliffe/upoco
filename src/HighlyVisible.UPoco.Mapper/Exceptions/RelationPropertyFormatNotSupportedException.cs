﻿using System;

namespace HighlyVisible.UPoco.Mapper.Exceptions
{
    /// <summary>
    /// The value of a content property which is being mapped by the mapping engine
    /// cannot be parsed to IDs.
    /// </summary>
    public class RelationPropertyFormatNotSupportedException : Exception
    {
        /// <param name="propertyValue">The unsupported value of the property.</param>
        /// <param name="destinationPropertyType">The destination property type.</param>
        public RelationPropertyFormatNotSupportedException(string propertyValue, Type destinationPropertyType)
            : base(string.Format(@"Could not parse '{0}' into integer IDs for destination type '{1}'.  
Trying storing your relation properties as CSV (e.g. '1234,2345,4576')", propertyValue, destinationPropertyType.FullName))
        {
        }
    }
}