﻿namespace HighlyVisible.UPoco.Mapper.Exceptions
{
  using System;

  /// <summary>
    /// No map exists for this engine for the destination type
    /// </summary>
    public class MapNotFoundException : Exception
    {
        /// <param name="destinationType">The requested and unfound destination type</param>
        public MapNotFoundException(Type destinationType)
            : base(string.Format(@"No map could be found for type '{0}'.  Remember
to run CreateMap for every model type you are using.", destinationType.FullName))
        {
        }
    }
}