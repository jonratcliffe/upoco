﻿using System;

namespace HighlyVisible.UPoco.Mapper.Exceptions
{
    /// <summary>
    /// A collection which cannot be instiatated/populated by the mapping engine
    /// is used.
    /// </summary>
    public class CollectionTypeNotSupportedException : Exception
    {
        /// <param name="type">The unsupported collection type.</param>
        public CollectionTypeNotSupportedException(Type type)
            : base(string.Format(
                @"Could not map to collection of type '{0}'.  
The property type must be assignable from IEnumerable<{1}>
or have a constructor which takes a single argument of IEnumerable<{1}>
(such as a List<{1}>).",
                type.FullName,
                type.Name))
        {
        }
    }
}