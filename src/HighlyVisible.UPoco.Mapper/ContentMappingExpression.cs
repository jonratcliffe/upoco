﻿namespace HighlyVisible.UPoco.Mapper
{
  using System;
  using System.Collections.Generic;
  using System.Linq.Expressions;
  using System.Reflection;
  using Property;
  using Umbraco.Core.Models;

  /// <summary>
    /// Fluent configuration for a NodeMap.
    /// </summary>
    /// <typeparam name="TDestination">The destination model type</typeparam>
    internal class ContentMappingExpression<TDestination> : IContentMappingExpression<TDestination>
    {
        private ContentMappingEngine Engine { get; set; }
        private ContentMapper ContentMapper { get; set; }

        public ContentMappingExpression(ContentMappingEngine engine, ContentMapper mapping)
        {
            if (mapping == null)
            {
                throw new ArgumentNullException("mapping");
            }
            
            if (engine == null)
            {
                throw new ArgumentNullException("engine");
            }

            ContentMapper = mapping;
            Engine = engine;
        }

        #region Default properties

        public IContentMappingExpression<TDestination> DefaultProperty<TSourceProperty, TDestinationProperty>(
            Expression<Func<TDestination, TDestinationProperty>> destinationProperty,
            Expression<Func<IContent, TSourceProperty>> contentProperty,
            Func<TSourceProperty, TDestinationProperty> mapping
            )
        {
            if (mapping == null)
            {
                throw new ArgumentNullException("mapping");
            }
             
            if (destinationProperty == null)
            {
                throw new ArgumentNullException("destinationProperty");
            }
            
            if (contentProperty == null)
            {
                throw new ArgumentNullException("contentProperty");
            }

            var mapper = new DefaultPropertyMapper(
                ContentMapper,
                destinationProperty.GetPropertyInfo(),
                contentProperty.GetPropertyInfo(),
                x => (object)mapping((TSourceProperty)x)
                );

            ContentMapper.InsertPropertyMapper(mapper);

            return this;
        }

        #endregion

        #region Basic properties

        public IContentMappingExpression<TDestination> BasicProperty(
            Expression<Func<TDestination, object>> destinationProperty,
            string propertyAlias
            )
        {
            if (string.IsNullOrEmpty(propertyAlias))
            {
                throw new ArgumentException("Property alias cannot be null", "propertyAlias");
            }

            var mapper = new BasicPropertyMapper(
                null,
                null,
                ContentMapper,
                destinationProperty.GetPropertyInfo(),
                propertyAlias
                );

            ContentMapper.InsertPropertyMapper(mapper);

            return this;
        }

        public IContentMappingExpression<TDestination> BasicProperty<TSourceProperty>(
            Expression<Func<TDestination, object>> destinationProperty,
            BasicPropertyMapping<TSourceProperty> mapping,
            string propertyAlias = null
            )
        {
            if (mapping == null)
            {
                throw new ArgumentNullException("mapping");
            }

            var mapper = new BasicPropertyMapper(
                x => mapping((TSourceProperty)x),
                typeof(TSourceProperty),
                ContentMapper,
                destinationProperty.GetPropertyInfo(),
                propertyAlias
                );

            ContentMapper.InsertPropertyMapper(mapper);

            return this;
        }

        #endregion

        #region Single Properties

        public IContentMappingExpression<TDestination> SingleProperty(
            Expression<Func<TDestination, object>> destinationProperty,
            string propertyAlias
            )
        {
            if (string.IsNullOrEmpty(propertyAlias))
            {
                throw new ArgumentException("Property alias cannot be null", "propertyAlias");
            }
            
            if (destinationProperty == null)
            {
                throw new ArgumentNullException("destinationProperty");
            }

            var mapper = new SinglePropertyMapper(
                null,
                null,
                ContentMapper,
                destinationProperty.GetPropertyInfo(),
                propertyAlias
                );

            ContentMapper.InsertPropertyMapper(mapper);

            return this;
        }

        public IContentMappingExpression<TDestination> SingleProperty(
            Expression<Func<TDestination, object>> destinationProperty, 
            Func<int, int?> mapping
            )
        {
            if (mapping == null)
            {
                throw new ArgumentNullException("mapping");
            }
            
            if (destinationProperty == null)
            {
                throw new ArgumentNullException("destinationProperty");
            }

            var mapper = new SinglePropertyMapper(
                (context, sourceValue) => mapping(context.Id),
                null,
                ContentMapper,
                destinationProperty.GetPropertyInfo(),
                null
                );

            ContentMapper.InsertPropertyMapper(mapper);

            return this;
        }

        public IContentMappingExpression<TDestination> SingleProperty<TSourceProperty>(
            Expression<Func<TDestination, object>> destinationProperty,
            SinglePropertyMapping<TSourceProperty> mapping,
            string propertyAlias = null
            )
        {
            if (mapping == null)
            {
                throw new ArgumentNullException("mapping");
            }
            
            if (destinationProperty == null)
            {
                throw new ArgumentNullException("destinationProperty");
            }


            var mapper = new SinglePropertyMapper(
                (context, value) => mapping((TSourceProperty)value),
                typeof(TSourceProperty),
                ContentMapper,
                destinationProperty.GetPropertyInfo(),
                propertyAlias
                );

            ContentMapper.InsertPropertyMapper(mapper);

            return this;
        }

        #endregion

        #region Collection properties

        public IContentMappingExpression<TDestination> CollectionProperty(
            Expression<Func<TDestination, object>> destinationProperty,
            string propertyAlias
            )
        {
            if (string.IsNullOrEmpty(propertyAlias))
            {
                throw new ArgumentException("Property alias cannot be null", "propertyAlias");
            }

            var mapper = new CollectionPropertyMapper(
                null,
                null,
                ContentMapper,
                destinationProperty.GetPropertyInfo(),
                propertyAlias
                );

            ContentMapper.InsertPropertyMapper(mapper);

            return this;
        }

        public IContentMappingExpression<TDestination> CollectionProperty(
            Expression<Func<TDestination, object>> destinationProperty, 
            Func<int, IEnumerable<int>> mapping
            )
        {
            if (mapping == null)
            {
                throw new ArgumentNullException("mapping");
            }
            
            if (destinationProperty == null)
            {
                throw new ArgumentNullException("destinationProperty");
            }

            var mapper = new CollectionPropertyMapper(
                (context, sourceValue) => mapping(context.Id),
                null,
                ContentMapper,
                destinationProperty.GetPropertyInfo(),
                null
                );

            ContentMapper.InsertPropertyMapper(mapper);

            return this;
        }

        public IContentMappingExpression<TDestination> CollectionProperty<TSourceProperty>(
            Expression<Func<TDestination, object>> destinationProperty,
            CollectionPropertyMapping<TSourceProperty> mapping,
            string propertyAlias = null
            )
        {
            if (mapping == null)
            {
                throw new ArgumentNullException("mapping");
            }

            var mapper = new CollectionPropertyMapper(
                (context, value) => mapping((TSourceProperty)value),
                typeof(TSourceProperty),
                ContentMapper,
                destinationProperty.GetPropertyInfo(),
                propertyAlias
                );

            ContentMapper.InsertPropertyMapper(mapper);

            return this;
        }

        #endregion

        #region Custom properties

        public IContentMappingExpression<TDestination> CustomProperty(
            Expression<Func<TDestination, object>> destinationProperty,
            CustomPropertyMapping mapping,
            bool requiresInclude,
            bool allowCaching
            )
        {
            if (destinationProperty == null)
            {
                throw new ArgumentNullException("destinationProperty");
            }
            
            if (mapping == null)
            {
                throw new ArgumentNullException("mapping");
            }

            var mapper = new CustomPropertyMapper(
                mapping,
                requiresInclude,
                allowCaching,
                ContentMapper,
                destinationProperty.GetPropertyInfo()
                );

            ContentMapper.InsertPropertyMapper(mapper);

            return this;
        }

        #endregion

        /// <summary>
        /// Removes the mapping for a property, if any exists.
        /// </summary>
        /// <param name="destinationProperty">The property on the model to NOT map to</param>
        /// <exception cref="ArgumentNullException">If destinationProperty is null</exception>
        public IContentMappingExpression<TDestination> RemoveMappingForProperty(
            Expression<Func<TDestination, object>> destinationProperty
            )
        {
            var propertyInfo = destinationProperty.GetPropertyInfo();
            if (!propertyInfo.CanWrite)
            {
                throw new NotSupportedException(string.Format("Cannot remove mapping for a ReadOnly property. Name: '{0}', Class: '{1}'", propertyInfo.Name, propertyInfo.DeclaringType.FullName));
            }

            if (destinationProperty == null)
            {
                throw new ArgumentNullException("destinationProperty");
            }

            ContentMapper.RemovePropertyMapper(propertyInfo);

            return this;
        }
    }

    /// <summary>
    /// Thrown when a model property could not be mapped to a document type property.
    /// </summary>
    public class PropertyAliasNotFoundException : Exception
    {
        /// <summary>
        /// Instantiates the exception.
        /// </summary>
        public PropertyAliasNotFoundException(Type destinationType, PropertyInfo property, string documentTypeAlias)
            : base(string.Format("Could not map {0}'s {1} property to a property on document type of {2}.", destinationType.FullName, property.Name, documentTypeAlias))
        {
        }
    }

    /// <summary>
    /// Thrown when a model property could not be mapped to a default Node property.
    /// </summary>
    public class DefaultPropertyTypeException : Exception
    {
        /// <summary>
        /// Instantiates the exception.
        /// </summary>
        public DefaultPropertyTypeException(Type destinationType, PropertyInfo destinationProperty, PropertyInfo nodeProperty)
            : base(string.Format("Could not map {0}'s {1} property to a default Content property {2}.", destinationType.FullName, destinationProperty.Name, nodeProperty.Name))
        {
        }
    }
}
