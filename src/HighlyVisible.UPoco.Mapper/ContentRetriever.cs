﻿namespace HighlyVisible.UPoco.Mapper
{
  using System.Linq;
  using Umbraco.Core;
  using Umbraco.Core.Models;
  using Umbraco.Core.Services;

  public class ContentRetriever : IContentRetriever
    {
        private readonly IContentService _contentService;
        private readonly CacheHelper _cacheHelper;

        public ContentRetriever()
        {
            _contentService = ApplicationContext.Current.Services.ContentService;
            _cacheHelper = ApplicationContext.Current.ApplicationCache;
        }

        public IContent GetById(int id, PreviewState previewState)
        {
            var foundContent = _contentService.GetById(id);


            if (ShouldShowPublishedVersion(foundContent, previewState))
            {
                foundContent = GetPublishedVersionOfContent(_contentService.GetById(id));
            }
            else
            {
                ClearItemFromCache(id);
                foundContent = _contentService.GetById(id);
            }

            ParseShortcodes(foundContent);

            return foundContent;
        }

        private void ClearItemFromCache(int id)
        {
            _cacheHelper.ClearCacheItem(GenerateCacheKey(id));
        }

        private string GenerateCacheKey(int id)
        {
            return string.Format("umbrtmche_{0}-{1}", typeof(IContent).Name, id.ToGuid());
        }

        private IContent GetPublishedVersionOfContent(IContent content)
        {
            if (content.Published) return content; //this is the published version

            //try and get the last published version (in cases where content has pending changes)
            if (_contentService.HasPublishedVersion(content.Id))
                return _contentService.GetPublishedVersion(content.Id);

            //content has never been published
            return null;
        }

        private bool ShouldShowPublishedVersion(IContent content, PreviewState previewState)
        {
            var shouldShowPublishedVersion = true;

            if (previewState.IsInPreviewMode)
            {
                if (previewState.IsGlobalPreview)
                {
                    if (content.Path.Split(',').Any(c => c == previewState.OriginatingContentId.ToString()))
                    {
                        shouldShowPublishedVersion = false;
                    }
                }
                else
                {
                    if (previewState.OriginatingContentId == content.Id)
                    {
                        shouldShowPublishedVersion = false;
                    }
                }
            }

            return shouldShowPublishedVersion;
        }

        private void ParseShortcodes(IContent content)
        {
            if (!Mapper.Engine.Shortcodes.Any()) return;

            foreach (var property in content.Properties)
            {
                if (property.Value is string)
                {
                    property.Value = ReplaceShortcodesInString(property.Value.ToString());
                }
            }
        }

        private string ReplaceShortcodesInString(string inputString)
        {
            foreach (var shortcode in Mapper.Engine.Shortcodes)
            {
                inputString = inputString.Replace(shortcode.Key, shortcode.Value);
            }

            return inputString;
        }
    }
}