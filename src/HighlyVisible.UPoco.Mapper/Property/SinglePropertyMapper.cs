﻿namespace HighlyVisible.UPoco.Mapper.Property
{
  using System;
  using System.Linq;
  using System.Reflection;
  using Umbraco.Core;

  internal class SinglePropertyMapper : PropertyMapperBase
    {
        private readonly Func<ContentMappingContext, object, int?> _mapping; // context, source property value, single relationship ID
        private readonly Type _sourcePropertyType;

        /// <summary>
        /// Maps a single relationship.
        /// </summary>
        /// <param name="mapping">
        /// Mapping to a nullable node ID.  Takes the context and source property value
        /// as parameters.
        /// If <c>null</c>, the mapping will be deduced from the other parameters.
        /// </param>
        /// <param name="sourcePropertyType">
        /// The type of object being supplied to <paramref name="mapping"/>.
        /// Will be set to <c>int?</c> if <paramref name="mapping"/> is specified.
        /// </param>
        /// <param name="sourcePropertyAlias">
        /// The alias of the node property to map from.  If null, the closest ancestor which is 
        /// compatible with <paramref name="destinationProperty"/> will be mapped instead.
        /// </param>
        /// <param name="contentMapper"></param>
        /// <param name="destinationProperty"></param>
        public SinglePropertyMapper(
            Func<ContentMappingContext, object, int?> mapping,
            Type sourcePropertyType,
            ContentMapper contentMapper,
            PropertyInfo destinationProperty,
            string sourcePropertyAlias
            )
            : base(contentMapper, destinationProperty)
        {
            if (sourcePropertyType == null && mapping != null)
            {
                // Default source property type
                sourcePropertyType = typeof(int?);
            }
            
            if (sourcePropertyAlias == null
                && mapping != null
                && !typeof(int?).IsAssignableFrom(sourcePropertyType))
            {
                throw new ArgumentException("If specifying a mapping for a single model with no property alias, the source property type must be assignable to Nullable<int>.");
            }

            SourcePropertyAlias = sourcePropertyAlias;
            RequiresInclude = true;
            AllowCaching = true;
            _mapping = mapping;
            _sourcePropertyType = sourcePropertyType;
        }

        public override object MapProperty(ContentMappingContext context)
        {
            int? id = null;

            // Get ID
            if (AllowCaching
                && Engine.CacheProvider != null
                && Engine.CacheProvider.ContainsPropertyValue(context.Id, DestinationInfo.Name)
                && !context.PreviewState.IsInPreviewMode)
            {
                id = Engine.CacheProvider.GetPropertyValue(context.Id, DestinationInfo.Name) as int?;
            }
            else
            {
                var content = context.GetContent();

                if (content == null || string.IsNullOrEmpty(content.Name))
                {
                    throw new InvalidOperationException("Node cannot be null or empty");
                }

                if (string.IsNullOrEmpty(SourcePropertyAlias))
                {
                    // Get closest parent
                    var aliases = Engine
                        .GetCompatibleContentTypeAliases(DestinationInfo.PropertyType)
                        .ToArray();
                    var ancestorNode = ApplicationContext.Current.Services.ContentService.GetAncestors(content)
                        .FirstOrDefault(x => aliases.Contains(x.ContentType.Alias));

                    if (ancestorNode != null)
                    {
                        // Found one
                        id = ancestorNode.Id;

                        context.AddContentToContextCache(ancestorNode);
                    }

                    if (_mapping != null)
                    {
                        id = _mapping(context, id);
                    }
                }
                else
                {
                    if (_mapping == null)
                    {
                        // Map ID from node property
                        id = GetSourcePropertyValue<int?>(content);
                    }
                    else
                    {
                        // Custom mapping
                        id = _mapping(context, GetSourcePropertyValue(content, _sourcePropertyType));
                    }
                }

                if (AllowCaching
                    && Engine.CacheProvider != null
                    && !context.PreviewState.IsInPreviewMode)
                {
                    Engine.CacheProvider.InsertPropertyValue(context.Id, DestinationInfo.Name, id);
                }
            }

            if (!id.HasValue)
            {
                // Not found
                return null;
            }

            // Map to model
            var childPaths = GetNextLevelPaths(context.Paths);
            var childContext = new ContentMappingContext(id.Value, childPaths, context, context.PreviewState);

            return Engine.Map(childContext, DestinationInfo.PropertyType, context.PreviewState);
        }
    }
}
