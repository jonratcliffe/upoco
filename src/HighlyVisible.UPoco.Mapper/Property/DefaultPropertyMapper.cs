﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Umbraco.Core.Models;

namespace HighlyVisible.UPoco.Mapper.Property
{
    internal class DefaultPropertyMapper : PropertyMapperBase
    {
        private readonly Func<object, object> _mapping;
        private readonly PropertyInfo _nodeProperty;

        public DefaultPropertyMapper(
            ContentMapper contentMapper,
            PropertyInfo destinationProperty,
            PropertyInfo nodeProperty,
            Func<object, object> mapping
            )
            :base(contentMapper, destinationProperty)
        {
            if (nodeProperty == null)
            {
                throw new ArgumentNullException("nodeProperty");
            }

            if (!destinationProperty.PropertyType.IsAssignableFrom(nodeProperty.PropertyType))
            {
                throw new DefaultPropertyTypeException(contentMapper.DestinationType, destinationProperty, nodeProperty);
            }

            RequiresInclude = false;
            AllowCaching = true;
            _mapping = mapping;
            _nodeProperty = nodeProperty;
        }

        public override object MapProperty(ContentMappingContext context)
        {
            object value = null;

            // Check cache
            if (AllowCaching
                && Engine.CacheProvider != null 
                && Engine.CacheProvider.ContainsPropertyValue(context.Id, DestinationInfo.Name)
                && !context.PreviewState.IsInPreviewMode)
            {
                value = Engine.CacheProvider.GetPropertyValue(context.Id, DestinationInfo.Name);
            }
            else
            {
                var content = context.GetContent();

                if (content == null || string.IsNullOrEmpty(content.Name))
                {
                    throw new InvalidOperationException("Node cannot be null or empty");
                }

                value = _nodeProperty.GetValue(content, null);

                if (_mapping != null)
                {
                    value = _mapping(value);
                }

                if (AllowCaching
                    && Engine.CacheProvider != null
                    && !context.PreviewState.IsInPreviewMode)
                {
                    Engine.CacheProvider.InsertPropertyValue(context.Id, DestinationInfo.Name, value);
                }
            }

            return value;
        }

        /// <summary>
        /// Given a content property name, returns the content property info if
        /// there is a match again the properties of <c>IContent</c>.
        /// 
        /// Checks whether the content's property is assignable to the destination
        /// property.
        /// </summary>
        /// <param name="destinationProperty">The model property.</param>
        /// <returns>The property or <c>null</c> if no match was found.</returns>
        public static PropertyInfo GetDefaultMappingForName(PropertyInfo destinationProperty)
        {
            Expression<Func<IContent, object>> propertyExpression = null;

            switch (destinationProperty.Name.ToLowerInvariant())
            {
                case "createdate":
                    propertyExpression = n => n.CreateDate;
                    break;
                case "creatorid":
                    propertyExpression = n => n.GetCreatorProfile().Id;
                    break;
                case "creatorname":
                    propertyExpression = n => n.GetCreatorProfile().Name;
                    break;
                case "id":
                    propertyExpression = n => n.Id;
                    break;
                case "level":
                    propertyExpression = n => n.Level;
                    break;
                case "name":
                    propertyExpression = n => n.Name;
                    break;
                case "nodetypealias":
                    propertyExpression = n => n.ContentType;
                    break;
                case "path":
                    propertyExpression = n => n.Path;
                    break;
                case "sortorder":
                    propertyExpression = n => n.SortOrder;
                    break;
                case "template":
                    propertyExpression = n => n.Template;
                    break;
                case "updatedate":
                    propertyExpression = n => n.UpdateDate;
                    break;
                case "version":
                    propertyExpression = n => n.Version;
                    break;
                case "writerid":
                    propertyExpression = n => n.WriterId;
                    break;
                case "writername":
                    propertyExpression = n => n.GetWriterProfile().Name;
                    break;
            }

            if (propertyExpression != null)
            {
                var nodeProperty = propertyExpression.GetPropertyInfo();

                if (destinationProperty.PropertyType.IsAssignableFrom(nodeProperty.PropertyType))
                {
                    return nodeProperty;
                }
            }

            return null;
        }
    }
}
