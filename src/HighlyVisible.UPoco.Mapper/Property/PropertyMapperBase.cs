﻿namespace HighlyVisible.UPoco.Mapper.Property
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Reflection;
  using Exceptions;
  using Umbraco.Core.Models;

  /// <summary>
    /// An immutable mapper for Umbraco Content properties to strongly typed model properties
    /// </summary>
    public abstract class PropertyMapperBase
    {

        /// <summary>
        /// Whether the property mapper should allow its mapped value to be cached.
        /// </summary>
        protected bool AllowCaching { get; set; }

        protected ContentMappingEngine Engine { get; set; }
        protected ContentMapper ContentMapper { get; set; }
        protected string SourcePropertyAlias { get; set; }

        public PropertyInfo DestinationInfo { get; private set; }
        public bool RequiresInclude { get; protected set; }

        /// <param name="destinationProperty">
        /// Describes the model property being mapped to.
        /// </param>
        /// <param name="contentMapper">
        /// The content mapper using this property mapper.
        /// </param>
        public PropertyMapperBase(
            ContentMapper contentMapper,
            PropertyInfo destinationProperty
            )
        {
            if (contentMapper == null)
            {
                throw new ArgumentNullException("contentMapper");
            }
            else if (destinationProperty == null)
            {
                throw new ArgumentNullException("destinationProperty");
            }

            ContentMapper = contentMapper;
            Engine = contentMapper.Engine;
            DestinationInfo = destinationProperty;
        }

        /// <summary>
        /// Maps a content property.
        /// </summary>
        /// <param name="context">The context describing the mapping.</param>
        /// <returns>The strongly typed, mapped property.</returns>
        public abstract object MapProperty(ContentMappingContext context);

        /// <summary>
        /// Gets the paths relative to the property being mapped.
        /// </summary>
        protected string[] GetNextLevelPaths(string[] paths)
        {
            if (paths == null)
            {
                // No paths
                return new string[0];
            }

            var pathsForProperty = new List<string>();

            foreach (var path in paths)
            {
                if (string.IsNullOrEmpty(path))
                {
                    throw new ArgumentException("No path can be null or empty", "paths");
                }

                var segments = path.Split('.');

                if (segments.First() == DestinationInfo.Name
                    && segments.Length > 1)
                {
                    pathsForProperty.Add(string.Join(".", segments.Skip(1)));
                }
            }

            return pathsForProperty.ToArray();
        }

        /// <summary>
        /// Gets an IContent's property as a certain type.
        /// </summary>
        /// <param name="sourcePropertyType">The type to get the property as (should be a system type or enum)</param>
        /// <param name="content">The content to get the property value of</param>
        public object GetSourcePropertyValue(IContent content, Type sourcePropertyType)
        {
            if (content == null || string.IsNullOrEmpty(content.Name))
            {
                throw new ArgumentException("Content cannot be null or empty", "content");
            }
            
            if (sourcePropertyType == null)
            {
                throw new ArgumentNullException("sourcePropertyType");
            }
            
            if (string.IsNullOrEmpty(SourcePropertyAlias))
            {
                throw new InvalidOperationException("SourcePropertyAlias cannot be null or empty");
            }

            if (!content.HasProperty(SourcePropertyAlias))
            {
                throw new KeyNotFoundException(string.Format("Property '{0}' was not found on content id {1} of type {2}", SourcePropertyAlias, content.Id, content.ContentType.Alias));
            }

            // CSV of IDs
            // TODO: can GetProperty<> handle this type?
            if ((typeof(IEnumerable<int>)).IsAssignableFrom(sourcePropertyType))
            {
                var csv = content.Properties[SourcePropertyAlias].Value as string;
                var ids = new List<int>();

                if (!string.IsNullOrWhiteSpace(csv))
                {
                    foreach (var idString in csv.Split(','))
                    {
                        // Ensure this is actually a list of content IDs
                        int id;
                        if (!int.TryParse(idString.Trim(), out id))
                        {
                            throw new RelationPropertyFormatNotSupportedException(csv, DestinationInfo.DeclaringType);
                        }

                        ids.Add(id);
                    }
                }

                return ids;
            }

            // Handle booleans (which actually, unhelpfully come back from Umbraco as integers)
            if(sourcePropertyType == typeof(Boolean))
            {
                return content.Properties[SourcePropertyAlias].Value.Equals(1);
            }

            // Handle cases where type is integer but value coming from umbraco is empty
            if (sourcePropertyType == typeof (int) && string.IsNullOrWhiteSpace(content.Properties[SourcePropertyAlias].Value.ToString()))
            {
                return 0;
            }


            // Other type
            return content.Properties[SourcePropertyAlias].Value;
        }

        /// <summary>
        /// Shorthand for <see cref="GetSourcePropertyValue"/>
        /// </summary>
        public T GetSourcePropertyValue<T>(IContent content)
        {
            return (T)GetSourcePropertyValue(content, typeof(T));
        }
    }
}
