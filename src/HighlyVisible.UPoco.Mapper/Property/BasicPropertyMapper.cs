﻿namespace HighlyVisible.UPoco.Mapper.Property
{
  using System;
  using System.Reflection;

  internal class BasicPropertyMapper : PropertyMapperBase
    {
        private readonly Func<object, object> _mapping;
        private readonly Type _sourcePropertyType;

        /// <summary>
        /// Maps a basic property value.
        /// </summary>
        /// <param name="mapping">
        /// The mapping for the property value.  Cannot be null.
        /// </param>
        /// <param name="sourcePropertyType">
        /// The type of the first parameter being supplied to <paramref name="mapping"/>.
        /// Cannot be <c>null</c>.
        /// </param>
        /// <param name="sourcePropertyAlias">
        /// The alias of the node property to map from.  Required.
        /// </param>
        /// <param name="contentMapper"></param>
        /// <param name="destinationProperty"></param>
        public BasicPropertyMapper(
            Func<object, object> mapping,
            Type sourcePropertyType,
            ContentMapper contentMapper,
            PropertyInfo destinationProperty,
            string sourcePropertyAlias
            )
            :base(contentMapper, destinationProperty)
        {
            if (sourcePropertyType == null && mapping != null)
            {
                throw new ArgumentNullException("sourcePropertyType", "Source property type must be specified when using a mapping");
            }

            if (sourcePropertyAlias == null)
            {
                sourcePropertyAlias = ContentMapper.GetPropertyAlias(destinationProperty);

                if (sourcePropertyAlias == null)
                {
                    throw new PropertyAliasNotFoundException(sourcePropertyType, destinationProperty, sourcePropertyAlias);
                }
            }

            SourcePropertyAlias = sourcePropertyAlias;
            RequiresInclude = false;
            AllowCaching = true;
            _mapping = mapping;
            _sourcePropertyType = sourcePropertyType;
        }

        public override object MapProperty(ContentMappingContext context)
        {
            object value = null;

            // Check cache
            if (AllowCaching
                && Engine.CacheProvider != null 
                && Engine.CacheProvider.ContainsPropertyValue(context.Id, DestinationInfo.Name)
                && !context.PreviewState.IsInPreviewMode)
            {
                value = Engine.CacheProvider.GetPropertyValue(context.Id, DestinationInfo.Name);
            }
            else
            {
                var node = context.GetContent();

                if (node == null || string.IsNullOrEmpty(node.Name))
                {
                    throw new InvalidOperationException("Node cannot be null or empty");
                }

                if (_mapping == null)
                {
                    // Map straight to property
                    value = GetSourcePropertyValue(node, DestinationInfo.PropertyType);
                }
                else
                {
                    // Custom mapping
                    var sourceValue = GetSourcePropertyValue(node, _sourcePropertyType);
                    value = _mapping(sourceValue);
                }

                if (AllowCaching
                    && Engine.CacheProvider != null 
                    && !context.PreviewState.IsInPreviewMode)
                {
                    Engine.CacheProvider.InsertPropertyValue(context.Id, DestinationInfo.Name, value);
                }
            }

            return value;
        }
    }
}
