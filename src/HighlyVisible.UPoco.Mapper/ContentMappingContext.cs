﻿namespace HighlyVisible.UPoco.Mapper
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using Umbraco.Core.Models;

  /// <summary>
    /// Provides a context for a stack of mapping operations.  Lifetime should complete
    /// once the bottom mapping operation is completed.
    /// </summary>
    public class ContentMappingContext
    {
        /// <summary>
        /// The ID of the content being mapped.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The paths to be mapped.
        /// </summary>
        public string[] Paths { get; set; }

        /// <summary>
        /// The context which spawned this context
        /// </summary>
        public ContentMappingContext ParentContext { get; set; }

        public PreviewState PreviewState { get; set; }

        private readonly IContentRetriever _contentRetriever;

        public ContentMappingContext(int id, string[] paths, ContentMappingContext parent, PreviewState previewState)
        {
            Id = id;
            Paths = paths;
            ParentContext = parent;
            PreviewState = previewState;

            _contentCache = new List<IContent>();
            _contentRetriever = new ContentRetriever();
        }

        public ContentMappingContext(IContent content, string[] paths, ContentMappingContext parent, PreviewState previewState)
            : this(content.Id, paths, parent, previewState)
        {
            _contentCache.Add(content);
        }

        /// <summary>
        /// Gets the content being mapped.
        /// </summary>
        public IContent GetContent()
        {
            IContent foundContent = null;

            if (!PreviewState.IsInPreviewMode)
            {
                foundContent = GetContentFromContextCache(Id);
            }

            if (foundContent == null)
            {
                foundContent = _contentRetriever.GetById(Id, PreviewState);

                if (string.IsNullOrEmpty(foundContent.Name))
                {
                    return null;
                }
            }

            AddContentToContextCache(foundContent);

            return foundContent;
        }

        #region Context cache

        /// <summary>
        /// Stores the content which have been cached during this operation.
        /// </summary>
        private readonly List<IContent> _contentCache;

        /// <summary>
        /// Looks for a <c>Node</c> in the context tree.
        /// </summary>
        /// <param name="id">The ID of the content.</param>
        /// <returns>The content, or null if not found.</returns>
        public IContent GetContentFromContextCache(int id)
        {
            var currentContext = this;

            while (currentContext.ParentContext != null)
            {
                var foundNode = currentContext._contentCache.FirstOrDefault(n => n.Id == id);

                if (foundNode != null)
                {
                    return foundNode;
                }

                currentContext = currentContext.ParentContext;
            }

            return null;
        }

        /// <summary>
        /// Adds a content to the context cache.
        /// </summary>
        public void AddContentToContextCache(IContent content)
        {
            if (content == null
                || string.IsNullOrEmpty(content.Name))
            {
                return;
            }

            _contentCache.Add(content);
        }

        /// <summary>
        /// Adds a collection of content to the context cache.
        /// </summary>
        public void AddContentToContextCache(IEnumerable<IContent> content)
        {
            if (content == null)
            {
                throw new ArgumentNullException("content");
            }

            foreach (var node in content)
            {
                AddContentToContextCache(node);
            }
        }

        #endregion
    }
}
