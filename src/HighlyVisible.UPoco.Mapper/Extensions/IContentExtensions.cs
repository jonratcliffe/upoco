﻿using Umbraco.Core.Models;

namespace HighlyVisible.UPoco.Mapper.Extensions
{
    public static class IContentExtensions
    {
        public static string GetNiceUrl(this IContent content)
        {
            return content.Id.ToString();
        }
    }
}