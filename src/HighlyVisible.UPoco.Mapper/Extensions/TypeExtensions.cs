﻿using System;
using System.Collections;
using System.Collections.Generic;
using HighlyVisible.UPoco.Mapper.Enums;
using Umbraco.Core.Models;

namespace HighlyVisible.UPoco.Mapper.Extensions
{
    internal static class TypeExtensions
    {
        /// <summary>
        /// Gets the classification for a mapped property type as far is mapping is concerned.
        /// </summary>
        public static MappedPropertyType GetMappedPropertyType(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            if (type == typeof (ITemplate))
            {
                return MappedPropertyType.Default;
            }

            var isString = (type == typeof(string));
            var isEnumerable = typeof(IEnumerable).IsAssignableFrom(type);
            var isDictionary = type.FullName.StartsWith(typeof(IDictionary).FullName)
                               || type.FullName.StartsWith(typeof(IDictionary<,>).FullName)
                               || type.FullName.StartsWith(typeof(Dictionary<,>).FullName);
            var isClr = (type.Module.ScopeName == "CommonLanguageRuntimeLibrary");

            if (!isString && !isDictionary && isEnumerable)
            {
                return MappedPropertyType.Collection;
            }
            
            if (type.Module.ScopeName == "CommonLanguageRuntimeLibrary" || type.IsEnum)
            {
                return MappedPropertyType.SystemOrEnum;
            }

            // Fallback when class is not recognised.
            return MappedPropertyType.Model;
        }
    }
}