﻿namespace HighlyVisible.UPoco.Mapper
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Web.Caching;
  using Exceptions;
  using umbraco;
  using Umbraco.Core;
  using Umbraco.Core.Models;

  /// <summary>
  /// Handles the creation of map and the mapping of Umbraco <c>Node</c>s to strongly typed
  /// models.
  /// </summary>
  public class ContentMappingEngine : IContentMappingEngine
  {
    public Dictionary<Type, ContentMapper> ContentMappers { get; set; }

    private ICacheProvider _cacheProvider;
    private content.DocumentCacheEventHandler _documentCacheEventHandler;

    /// <summary>
    /// Instantiates a new NodeMappingEngine
    /// </summary>
    public ContentMappingEngine()
    {
      ContentMappers = new Dictionary<Type, ContentMapper>();
      Shortcodes = new Dictionary<string, string>();
    }


    /// <summary>
    /// Instantiates a new NodeMappingEngine using a web cache.
    /// </summary>
    /// <exception cref="ArgumentNullException">
    /// If <paramref name="cache"/> is null.
    /// </exception>
    public ContentMappingEngine(Cache cache)
    {
      if (cache == null)
      {
        throw new ArgumentNullException("cache");
      }

      SetCacheProvider(new DefaultCacheProvider(cache));
      Shortcodes = new Dictionary<string, string>();
    }

    /// <see cref="IContentMappingEngine.IsCachingEnabled" />
    public bool IsCachingEnabled
    {
      get
      {
        return _cacheProvider != null;
      }
    }

    public ICacheProvider CacheProvider
    {
      get
      {
        return _cacheProvider;
      }
    }

    /// <see cref="IContentMappingEngine.SetCacheProvider"/>
    public void SetCacheProvider(ICacheProvider cacheProvider)
    {
      if (_cacheProvider != null)
      {
        _cacheProvider.Clear();

        content.AfterUpdateDocumentCache -= _documentCacheEventHandler;
        content.AfterClearDocumentCache -= _documentCacheEventHandler;

        _documentCacheEventHandler = null;
      }

      if (cacheProvider != null)
      {
        _documentCacheEventHandler = (sender, e) => cacheProvider.Clear();

        // TODO test this

        content.AfterUpdateDocumentCache += _documentCacheEventHandler;
        content.AfterClearDocumentCache += _documentCacheEventHandler;
      }

      _cacheProvider = cacheProvider;
    }



    /// <summary>
    /// Creates a map to a strong type from an Umbraco content type
    /// using the unqualified class name of <typeparamref name="TDestination"/> 
    /// as the content type alias.
    /// </summary>
    /// <typeparam name="TDestination">The type to map to.</typeparam>
    /// <returns>Further mapping configuration</returns>
    /// <exception cref="ContentTypeNotFoundException">
    /// If the content type with an alias of <typeparamref name="TDestination"/>'s
    /// class name could not be found
    /// </exception>
    public IContentMappingExpression<TDestination> CreateMap<TDestination>()
        where TDestination : class, new()
    {
      var destinationType = typeof(TDestination);

      return CreateMap<TDestination>(destinationType.Name);
    }

    /// <summary>
    /// Creates a map to a strong type from an Umbraco content type.
    /// </summary>
    /// <typeparam name="TDestination">The type to map to.</typeparam>
    /// <param name="contentTypeAlias">The content type alias to map from.</param>
    /// <returns>Further mapping configuration</returns>
    /// <exception cref="ContentTypeNotFoundException">
    /// If the <paramref name="contentTypeAlias"/> could not be found
    /// </exception>
    public IContentMappingExpression<TDestination> CreateMap<TDestination>(string contentTypeAlias)
        where TDestination : class, new()
    {
      var destinationType = typeof(TDestination);

      // Remove current mapping if any
      if (ContentMappers.ContainsKey(destinationType))
      {
        ContentMappers.Remove(destinationType);
      }

      // Get content type
      var contentType = ApplicationContext.Current.Services.ContentTypeService.GetAllContentTypes()
          .SingleOrDefault(
              d => string.Equals(d.Alias, contentTypeAlias, StringComparison.InvariantCultureIgnoreCase));

      if (contentType == null)
      {
        throw new ContentTypeNotFoundException(contentTypeAlias);
      }

      var contentMapper = new ContentMapper(this, destinationType, contentType);

      ContentMappers[destinationType] = contentMapper;

      if (_cacheProvider != null)
      {
        _cacheProvider.Clear();
      }

      return new ContentMappingExpression<TDestination>(this, contentMapper);
    }



    /// <summary>
    /// Gets an Umbraco <c>IContent</c> instance as a <paramref name="destinationType"/>, only including 
    /// specified relationship paths.
    /// </summary>
    /// <param name="sourceContent">The <c>IContent</c> to map from.</param>
    /// <param name="destinationType">The type to map to.</param>
    /// <param name="paths">The relationship paths to include, or null to include
    /// all relationship paths at the top level and none below.</param>
    /// <returns>
    /// <c>null</c> if the node does not exist or does not map to <paramref name="destinationType"/>.
    /// </returns>
    /// <exception cref="MapNotFoundException">If a suitable map for <paramref name="destinationType"/> has not 
    /// been created with <see cref="CreateMap()" />.</exception>
    public object Map(IContent sourceContent, Type destinationType, PreviewState previewState, string[] paths)
    {
      if (sourceContent == null || string.IsNullOrEmpty(sourceContent.Name))
      {
        return null;
      }

      var context = new ContentMappingContext(sourceContent, paths, null, previewState);

      return Map(context, destinationType, previewState);
    }

    /// <summary>
    /// Gets an Umbraco <c>IContent</c> instance as a <typeparamref name="TDestination"/>, only including 
    /// specified relationship paths.
    /// </summary>
    /// <typeparam name="TDestination">
    /// The type of object that <paramref name="sourceContent"/> maps to.
    /// </typeparam>
    /// <param name="sourceContent">The <c>IContent</c> to map from.</param>
    /// <param name="previewState"></param>
    /// <param name="paths">The relationship paths to include.</param>
    /// <returns><c>null</c> if the node does not exist.</returns>
    public TDestination Map<TDestination>(IContent sourceContent, PreviewState previewState, params string[] paths)
        where TDestination : class, new()
    {
      return (TDestination)Map(sourceContent, typeof(TDestination), previewState, paths);
    }

    /// <summary>
    /// Maps an IContent based on the <paramref name="context"/>.
    /// </summary>
    /// <param name="context">The context which describes the content mapping.</param>
    /// <param name="destinationType">The type to map to.</param>
    /// <returns><c>null</c> if the node does not exist.</returns>
    public object Map(ContentMappingContext context, Type destinationType, PreviewState previewState)
    {
      if (context == null)
      {
        throw new ArgumentNullException("context");
      }

      if (destinationType == null)
      {
        throw new ArgumentNullException("destinationType");
      }

      if (!ContentMappers.ContainsKey(destinationType))
      {
        throw new MapNotFoundException(destinationType);
      }

      string sourceContentTypeAlias = null;

      if (_cacheProvider != null && _cacheProvider.ContainsAlias(context.Id) && !previewState.IsInPreviewMode)
      {
        sourceContentTypeAlias = _cacheProvider.GetAlias(context.Id);

        if (sourceContentTypeAlias == null)
        {
          // Content does not exist
          return null;
        }
      }

      if (sourceContentTypeAlias == null)
      {
        var content = context.GetContent();

        if (content == null || string.IsNullOrEmpty(content.Name))
        {
          // Content doesn't exist
          if (_cacheProvider != null)
          {
            _cacheProvider.InsertAlias(context.Id, null);
          }

          return null;
        }

        if (_cacheProvider != null)
        {
          _cacheProvider.InsertAlias(context.Id, content.ContentType.Alias);
        }

        sourceContentTypeAlias = content.ContentType.Alias;
      }

      var nodeMapper = GetMapper(sourceContentTypeAlias, destinationType);

      if (nodeMapper == null)
      {
        return null;
      }

      return nodeMapper.MapNode(context);
    }

    /// <summary>
    /// Examines the engine's <see cref="ContentMappers"/> and returns content mapper
    /// which maps to the closest base class of <paramref name="type"/>.
    /// </summary>
    /// <returns>
    /// <c>null</c>  if there are no mappers which map to a base class of 
    /// <paramref name="type"/>.
    /// </returns>
    internal ContentMapper GetBaseNodeMapperForType(Type type)
    {
      var ancestorMappers = new List<ContentMapper>();

      foreach (var contentMapper in ContentMappers)
      {
        if (contentMapper.Value.DestinationType.IsAssignableFrom(type)
            && type != contentMapper.Value.DestinationType)
        {
          ancestorMappers.Add(contentMapper.Value);
        }
      }

      // Sort by inheritance
      ancestorMappers.Sort((x, y) =>
      {
        return x.DestinationType.IsAssignableFrom(y.DestinationType)
            ? 1
            : -1;
      });

      return ancestorMappers.FirstOrDefault();
    }

    /// <summary>
    /// Gets a content mapper which maps from <paramref name="sourceContentTypeAlias"/>
    /// to <paramref name="destinationType"/> or some class derived from 
    /// <paramref name="destinationType"/>.
    /// </summary>
    /// <param name="sourceContentTypeAlias">Content type alias to map from.</param>
    /// <param name="destinationType">The type which the mapped model must
    /// cast to.</param>
    /// <returns>The node mapper, or null if a suitable mapper could not be found</returns>
    internal ContentMapper GetMapper(string sourceContentTypeAlias, Type destinationType)
    {
      foreach (var contentMapper in ContentMappers)
      {
        if (destinationType.IsAssignableFrom(contentMapper.Value.DestinationType)
            && contentMapper.Value.SourceContentType.Alias == sourceContentTypeAlias)
        {
          return contentMapper.Value;
        }
      }

      return null;
    }

    /// <summary>
    /// Gets all content type aliases which can map to <paramref name="destinationType"/>.
    /// </summary>
    internal string[] GetCompatibleContentTypeAliases(Type destinationType)
    {
      var compatibleAliases = new List<string>();

      foreach (var contentMapper in ContentMappers)
      {
        if (destinationType.IsAssignableFrom(contentMapper.Value.DestinationType))
        {
          compatibleAliases.Add(contentMapper.Value.SourceContentType.Alias);
        }
      }

      return compatibleAliases.Distinct().ToArray();
    }


    /// <summary>
    /// Gets a query for content which maps to <typeparamref name="TDestination"/>.
    /// </summary>
    /// <typeparam name="TDestination">The type to map to.</typeparam>
    /// <returns>A fluent configuration for the query.</returns>
    /// <exception cref="MapNotFoundException">If a suitable map for <typeparamref name="TDestination"/> has not 
    /// been created with <see cref="CreateMap()" />.</exception>
    public IContentQuery<TDestination> Query<TDestination>()
        where TDestination : class, new()
    {
      return new ContentQuery<TDestination>(this);
    }

    public IDictionary<string, string> Shortcodes { get; set; }



  }
}
