﻿namespace HighlyVisible.UPoco.Mapper
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Web;
  using Exceptions;
  using Umbraco.Core.Models;

  /// <summary>
    /// Maps Umbraco <c>Node</c>s to strongly typed models.
    /// </summary>
    /// <remarks>
    /// Based on the uComponents package but updated to use Umbraco V6 API
    /// See http://ucomponents.org/umapper/ 
    /// </remarks>
    public static class Mapper
    {
        private static IContentMappingEngine _engine = new ContentMappingEngine();

        public static IContentMappingEngine Engine
        {
            get { return _engine; }
            set { _engine = value; }
        }

        /// <summary>
        /// Enables or disables caching, using the <c>HttpContext.Current.Cache</c>
        /// object.
        /// 
        /// Caching is disabled by default.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// If <c>HttpContext.Current</c> is not set.
        /// </exception>
        public static bool CachingEnabled
        {
            get
            {
                return _engine.IsCachingEnabled;
            }
            set
            {
                if (value)
                {
                    if (HttpContext.Current == null)
                    {
                        throw new InvalidOperationException("The current HttpContext is not available - caching cannot be enabled");
                    }

                    _engine.SetCacheProvider(new DefaultCacheProvider(HttpContext.Current.Cache));
                }
                else
                {
                    _engine.SetCacheProvider(null);
                }
            }
        }

        public static void ClearCache()
        {
            if (_engine.CacheProvider != null)
            {
                _engine.CacheProvider.Clear();
            }
        }

        public static IDictionary<string, string> Shortcodes
        {
            set { _engine.Shortcodes = value; }
        } 

        /// <summary>
        /// Creates a map to <typeparamref name="TDestination" /> from an Umbraco document type, 
        /// which must have the same alias as the unqualified class name of 
        /// <typeparamref name="TDestination"/>.
        /// </summary>
        /// <typeparam name="TDestination">The type to map to.</typeparam>
        /// <returns>Fluent configuration for the newly created mapping.</returns>
        /// <exception cref="ContentTypeNotFoundException">If a suitable document type could not be found</exception>
        public static IContentMappingExpression<TDestination> CreateMap<TDestination>()
            where TDestination : class, new()
        {
            return _engine.CreateMap<TDestination>();
        }

        /// <summary>
        /// Creates a map to <typeparamref name="TDestination" /> from an Umbraco document type,
        /// specifying the document type alias.
        /// </summary>
        /// <typeparam name="TDestination">The type to map to.</typeparam>
        /// <param name="documentTypeAlias">The document type alias to map from.</param>
        /// <returns>Fluent configuration for the newly created mapping.</returns>
        /// <exception cref="ContentTypeNotFoundException">If the document type could not be found.</exception>
        public static IContentMappingExpression<TDestination> CreateMap<TDestination>(string documentTypeAlias)
            where TDestination : class, new()
        {
            return _engine.CreateMap<TDestination>(documentTypeAlias);
        }

        /// <summary>
        /// Maps an Umbraco <c>Node</c> to a new instance of <typeparamref name="TDestination"/>.
        /// </summary>
        /// <typeparam name="TDestination">The type to map to.</typeparam>
        /// <param name="sourceContent">The <c>Node</c> to map from.</param>
        /// <param name="includeRelationships">Whether to load relationships.</param>
        /// <returns>
        /// A new instance of <typeparamref name="TDestination"/>, or <c>null</c> if 
        /// <paramref name="sourceContent"/> is <c>null</c> or does not map to 
        /// <typeparamref name="TDestination"/>.
        /// </returns>
        /// <exception cref="MapNotFoundException">
        /// If a map for <typeparamref name="TDestination"/> has not 
        /// been created with <see cref="CreateMap()" />.
        /// </exception>
        public static TDestination Map<TDestination>(IContent sourceContent, PreviewState previewState, bool includeRelationships = true)
            where TDestination : class, new()
        {
            if (sourceContent == null || string.IsNullOrEmpty(sourceContent.Name))
            {
                return null;
            }

            var paths = includeRelationships
                ? null // all
                : new string[0]; // none

            var context = new ContentMappingContext(sourceContent, paths, null, previewState);

            return (TDestination)_engine.Map(context, typeof(TDestination), previewState);
        }

        /// <summary>
        /// Gets an Umbraco <c>Node</c> as a new instance of <typeparamref name="TDestination"/>.
        /// </summary>
        /// <typeparam name="TDestination">The type that the <c>Node</c> maps to.</typeparam>
        /// <param name="id">The ID of the <c>Node</c></param>
        /// <param name="includeRelationships">Whether to load all the <c>Node</c>'s relationships</param>
        /// <returns>
        /// <c>null</c> if the <c>Node</c> does not exist or does not map to 
        /// <typeparamref name="TDestination"/>.
        /// </returns>
        /// <exception cref="MapNotFoundException">
        /// If a map for <typeparamref name="TDestination"/> has not 
        /// been created with <see cref="CreateMap()" />.
        /// </exception>
        public static TDestination Find<TDestination>(int id, PreviewState previewState, bool includeRelationships = true)
        {
            var paths = includeRelationships
                ? null // all
                : new string[0]; // none

            var context = new ContentMappingContext(id, paths, null, previewState);

            return (TDestination)_engine.Map(context, typeof(TDestination), previewState);
        }

        /// <summary>
        /// Gets all Umbraco <c>Node</c>s which map to <typeparamref name="TDestination"/> (including nodes which
        /// map to a class which derives from <typeparamref name="TDestination"/>).
        /// </summary>
        /// <typeparam name="TDestination">The type for the <c>Node</c>s to map to.</typeparam>
        /// <param name="includeRelationships">Whether to load all the <c>Node</c>s' relationships</param>
        /// <exception cref="MapNotFoundException">
        /// If a map for <typeparamref name="TDestination"/> has not 
        /// been created with <see cref="CreateMap()" />.
        /// </exception>
        public static IEnumerable<TDestination> GetAll<TDestination>(bool includeRelationships = true)
            where TDestination : class, new()
        {
            var destinationType = typeof(TDestination);

            if (!_engine.ContentMappers.ContainsKey(destinationType))
            {
                throw new MapNotFoundException(destinationType);
            }

            var paths = includeRelationships
                ? _engine.ContentMappers[destinationType]
                    .PropertyMappers
                    .Where(x => x.RequiresInclude)
                    .Select(x => x.DestinationInfo.Name)
                    .ToArray() // all
                : new string[0]; // none

            var query = Query<TDestination>();

            foreach (var path in paths)
            {
                query.Include(path);
            }

            return query;
        }

        /// <summary>
        /// Gets a query for nodes which map to <typeparamref name="TDestination"/>.
        /// </summary>
        /// <typeparam name="TDestination">The type to map to.</typeparam>
        /// <returns>A fluent configuration for the query.</returns>
        /// <exception cref="MapNotFoundException">
        /// If a suitable map for <typeparamref name="TDestination"/> has not 
        /// been created with <see cref="CreateMap()" />.
        /// </exception>
        public static IContentQuery<TDestination> Query<TDestination>()
            where TDestination : class, new()
        {
            return _engine.Query<TDestination>();
        }

    }
}
