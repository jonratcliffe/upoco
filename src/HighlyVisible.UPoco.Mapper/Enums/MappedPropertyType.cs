﻿namespace HighlyVisible.UPoco.Mapper.Enums
{
    internal enum MappedPropertyType
    {
        SystemOrEnum,
        Model,
        Collection,
        Default
    }
}