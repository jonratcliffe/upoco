﻿namespace HighlyVisible.UPoco.Mapper
{
  using System;
  using System.Collections;
  using System.Collections.Generic;
  using System.Linq;
  using System.Linq.Expressions;
  using Exceptions;
  using Umbraco.Core;
  using Umbraco.Core.Models;
  using Umbraco.Core.Services;

  /// <summary>
    /// Represents a query for mapped Umbraco content.  Enumerating the
    /// query gets mapped instances of every content which can be mapped to 
    /// <typeparamref name="TDestination"/>. 
    /// </summary>
    /// <typeparam name="TDestination">
    /// The type which queried content will be mapped to.
    /// </typeparam>
    internal class ContentQuery<TDestination> : IContentQuery<TDestination>
        where TDestination : class, new()
    {
        // Cache keys
        private const string ExplicitCacheFormat = "Explicit_{0}";
        private const string AllCacheFormat = "All_{0}";

        // The paths included in the query
        private readonly List<string> _paths;
        private readonly Dictionary<string, Func<object, bool>> _propertyFilters;
        private bool _isExplicit = false;

        // The engine which will execute the query
        private readonly ContentMappingEngine _engine;

        private readonly IContentService _contentService;
        private readonly IContentTypeService _contentTypeService;

        public ContentQuery(ContentMappingEngine engine)
        {
            var destinationType = typeof(TDestination);

            if (engine == null)
            {
                throw new ArgumentNullException("engine");
            }
            
            if (!engine.ContentMappers.ContainsKey(destinationType))
            {
                throw new MapNotFoundException(destinationType);
            }

            _engine = engine;
            _paths = new List<string>();
            _propertyFilters = new Dictionary<string, Func<object, bool>>();
            _contentService = ApplicationContext.Current.Services.ContentService;
            _contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
        }

        public IContentMappingEngine Engine
        {
            get
            {
                return _engine;
            }
        }

        #region Include

        public IContentQuery<TDestination> Include(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException("The path cannot be null or empty", "path");
            }

            if (!_paths.Contains(path))
            {
                _paths.Add(path);
            }

            return this;
        }

        public IContentQuery<TDestination> Include(Expression<Func<TDestination, object>> path)
        {
            if (path == null)
            {
                throw new ArgumentNullException("path");
            }

            string parsedPath;
            if (!path.Body.TryParsePath(out parsedPath)
                || parsedPath == null)
            {
                throw new ArgumentException(
                    string.Format("Path could not be parsed (got this far: '{0}')", parsedPath),
                    "path"
                    );
            }

            return Include(parsedPath);
        }

        public IContentQuery<TDestination> IncludeMany(string[] paths)
        {
            if (paths == null)
            {
                throw new ArgumentNullException("paths");
            }

            foreach (var path in paths)
            {
                Include(path);
            }

            return this;
        }

        public IContentQuery<TDestination> IncludeMany(Expression<Func<TDestination, object>>[] paths)
        {
            if (paths == null)
            {
                throw new ArgumentNullException("paths");
            }

            foreach (var path in paths)
            {
                Include(path);
            }

            return this;
        }

        #endregion

        #region Filtering

        public IContentQuery<TDestination> Explicit()
        {
            _isExplicit = true;

            return this;
        }

        public IContentQuery<TDestination> WhereProperty<TProperty>(
            Expression<Func<TDestination, TProperty>> property,
            Func<TProperty, bool> predicate
            )
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }
            
            if (predicate == null)
            {
                throw new ArgumentNullException("predicate");
            }

            var destinationType = typeof(TDestination);
            var destinationInfo = property.GetPropertyInfo();

            if (!_engine.ContentMappers[destinationType].PropertyMappers
                .Any(x => x.DestinationInfo.Name == destinationInfo.Name))
            {
                throw new PropertyNotMappedException(destinationType, destinationInfo.Name);
            }

            _propertyFilters.Add(destinationInfo.Name, x => predicate((TProperty)x));

            return this;
        }

        /// <summary>
        /// Filters a collection of mapping contexts based on the predicates in <see cref="_propertyFilters"/>.
        /// </summary>
        /// <param name="contexts">The mapping contexts to filter</param>
        /// <returns>The filtered subset of <paramref name="contexts"/>.</returns>
        private IEnumerable<ContentMappingContext> FilterSet(IEnumerable<ContentMappingContext> contexts)
        {
            if (contexts == null)
            {
                throw new ArgumentNullException();
            }

            var filteredContexts = new List<ContentMappingContext>(contexts);

            foreach (var filter in _propertyFilters)
            {
                var propertyMapper = _engine.ContentMappers[typeof(TDestination)]
                    .PropertyMappers
                    .Single(x => x.DestinationInfo.Name == filter.Key);

                filteredContexts.RemoveAll(context =>
                    {
                        var property = propertyMapper.MapProperty(context);
                        return !filter.Value(property);
                    });
            }

            return filteredContexts;
        }

        #endregion

        #region Execute

        public TDestination Map(IContent content, PreviewState previewState)
        {
            if (content == null || string.IsNullOrEmpty(content.Name))
            {
                return null;
            }

            var context = new ContentMappingContext(content, _paths.ToArray(), null, previewState);

            return (TDestination)_engine.Map(
                context,
                typeof(TDestination), 
                previewState
                );
        }

        public TDestination Find(int id, PreviewState previewState)
        {
            var context = new ContentMappingContext(id, _paths.ToArray(), null, previewState);

            return (TDestination)_engine.Map(
                context,
                typeof(TDestination),
                previewState
                );
        }

        /// <summary>
        /// Maps a collection of contexts to <typeparamref name="TDestination"/>.
        /// </summary>
        private IEnumerable<TDestination> Many(IEnumerable<ContentMappingContext> contexts, PreviewState previewState)
        {
            if (contexts == null)
            {
                throw new ArgumentNullException();
            }

            var filteredContexts = FilterSet(contexts);

            return filteredContexts.Select(c =>
                {
                    return (TDestination)_engine.Map(c, typeof(TDestination), previewState);
                });
        }

        public IEnumerable<TDestination> Many(IEnumerable<int> ids, PreviewState previewState)
        {
            if (ids == null)
            {
                throw new ArgumentNullException();
            }

            var paths = _paths.ToArray();
            var contexts = ids.Select(id => new ContentMappingContext(id, paths, null, previewState));

            return Many(contexts, previewState);
        }

        public IEnumerable<TDestination> Many(IEnumerable<IContent> content, PreviewState previewState)
        {
            if (content == null)
            {
                throw new ArgumentNullException("content");
            }

            var paths = _paths.ToArray();
            var contexts = content.Select(n => new ContentMappingContext(n, paths, null, previewState));

            return Many(contexts, previewState);
        }

        public IEnumerable<TProperty> SelectProperty<TProperty>(
            Expression<Func<TDestination, TProperty>> property,
            PreviewState previewState
            )
        {
            if (property == null)
            {
                throw new ArgumentNullException("property");
            }

            var sourceSet = EvaluateSourceSet(previewState);
            var propertyInfo = property.GetPropertyInfo();
            var propertyMapper = _engine.ContentMappers[typeof(TDestination)]
                .PropertyMappers
                .SingleOrDefault(x => x.DestinationInfo.Name == propertyInfo.Name);

            if (propertyMapper == null)
            {
                throw new PropertyNotMappedException(typeof(TDestination), propertyInfo.Name);
            }

            return sourceSet.Select(c => (TProperty)propertyMapper.MapProperty(c));
        }

        #endregion

        #region Enumeration

        /// <summary>
        /// Gets an enumerable representing the current source set of the query.
        /// </summary>
        private IEnumerable<ContentMappingContext> EvaluateSourceSet(PreviewState previewState)
        {
            var destinationType = typeof(TDestination);
            var paths = _paths.ToArray();
            IEnumerable<ContentMappingContext> sourceSet;

            if (!_engine.ContentMappers.ContainsKey(destinationType))
            {
                throw new MapNotFoundException(destinationType);
            }

            string cacheKey = string.Format(
                _isExplicit ? ExplicitCacheFormat : AllCacheFormat,
                destinationType.FullName
                );

            if (_engine.CacheProvider != null
                && _engine.CacheProvider.ContainsKey(cacheKey)
                && !previewState.IsInPreviewMode)
            {
                var ids = _engine.CacheProvider.Get(cacheKey) as int[];
                sourceSet = ids.Select(id => new ContentMappingContext(id, paths, null, previewState));
            }
            else
            {
                // Check whether to include derived maps
                var sourceNodeTypeAliases = _isExplicit
                    ? new[] { _engine.ContentMappers[destinationType].SourceContentType.Alias }
                    : _engine.GetCompatibleContentTypeAliases(destinationType);

                var contentItems = sourceNodeTypeAliases.SelectMany(GetContentByContentTypeAlias).ToList();

                if (_engine.CacheProvider != null && !previewState.IsInPreviewMode)
                {
                    // Cache the content IDs
                    _engine.CacheProvider.Insert(cacheKey, contentItems.Select(n => n.Id).ToArray());
                }

                sourceSet = contentItems.Select(n => new ContentMappingContext(n, paths, null, previewState));
            }

            return FilterSet(sourceSet);
        }

        private IEnumerable<IContent> GetContentByContentTypeAlias(string alias)
        {
            var contentTypeId = _contentTypeService.GetContentType(alias).Id;

            return _contentService.GetContentOfContentType(contentTypeId);
        }

        /// <summary>
        /// Gets and enumerator of mapped instances of every content which 
        /// can be mapped to <typeparamref name="TDestination"/>. 
        /// </summary>
        public IEnumerator<TDestination> GetEnumerator()
        {
            var sourceSet = EvaluateSourceSet(new PreviewState{IsInPreviewMode = false});
            var destinationSet = sourceSet.Select(c =>
                {
                    //TODO: Hard coded isInPreviewMode here - needs looking at.
                    return (TDestination)_engine.Map(c, typeof(TDestination), new PreviewState { IsInPreviewMode = false });
                });

            return destinationSet.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
