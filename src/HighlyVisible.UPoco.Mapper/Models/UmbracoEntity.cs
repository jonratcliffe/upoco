﻿namespace HighlyVisible.UPoco.Mapper.Models
{
	using System;

	public class UmbracoEntity
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime DateCreated { get; set; }
		public DateTime DateUpdated { get; set; }
		public int Level { get; set; }
	}
}