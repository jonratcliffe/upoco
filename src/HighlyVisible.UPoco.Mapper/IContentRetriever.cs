﻿namespace HighlyVisible.UPoco.Mapper
{
  using Umbraco.Core.Models;

  public interface IContentRetriever
    {
        IContent GetById(int id, PreviewState previewState);
    }
}