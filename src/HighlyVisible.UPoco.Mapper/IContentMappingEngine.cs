﻿namespace HighlyVisible.UPoco.Mapper
{
  using System;
  using System.Collections.Generic;
  using Umbraco.Core.Models;

  /// <summary>
    /// Handles the creation of map and the mapping of Umbraco Content to strongly typed
    /// models.
    /// </summary>
    public interface IContentMappingEngine
    {
        Dictionary<Type, ContentMapper> ContentMappers { get; set; }

        /// <summary>
        /// Creates a map to a strong type from an Umbraco document type
        /// using the unqualified class name of <typeparamref name="TDestination"/> 
        /// as the document type alias.
        /// </summary>
        /// <typeparam name="TDestination">The type to map to.</typeparam>
        /// <returns>Further mapping configuration</returns>
        IContentMappingExpression<TDestination> CreateMap<TDestination>() 
            where TDestination : class, new();

        /// <summary>
        /// Creates a map to a strong type from an Umbraco document type.
        /// </summary>
        /// <typeparam name="TDestination">The type to map to.</typeparam>
        /// <param name="contentTypeAlias">The document type alias to map from.</param>
        /// <returns>Further mapping configuration</returns>
        IContentMappingExpression<TDestination> CreateMap<TDestination>(string contentTypeAlias)
            where TDestination : class, new();

        /// <summary>
        /// True if the engine is in possession of an <see cref="ICacheProvider"/>.
        /// </summary>
        bool IsCachingEnabled { get; }

        /// <summary>
        /// Sets the cache provider for the engine to use. This will clear any existing
        /// cache provider.  Set as null to disable caching.
        /// 
        /// You probably want to use an instance of <see cref="DefaultCacheProvider"/>.
        /// </summary>
        void SetCacheProvider(ICacheProvider cacheProvider);

        ICacheProvider CacheProvider { get; }

        /// <summary>
        /// Gets a Umbraco Content as a <typeparamref name="TDestination"/>, only including 
        /// specified relationship paths.
        /// </summary>
        /// <typeparam name="TDestination">
        /// The type of object that <paramref name="sourceNode"/> maps to.
        /// </typeparam>
        /// <param name="sourceNode">The <c>Node</c> to map from.</param>
        /// <param name="paths">The relationship paths to include.</param>
        /// <returns><c>null</c> if the node does not exist.</returns>
        TDestination Map<TDestination>(IContent sourceNode, PreviewState previewState, params string[] paths)
            where TDestination : class, new();

        /// <summary>
        /// Maps an IContent based on the <paramref name="context"/>.
        /// </summary>
        /// <param name="context">The context which describes the content mapping.</param>
        /// <param name="destinationType">The type to map to.</param>
        /// <returns><c>null</c> if the node does not exist.</returns>
        object Map(ContentMappingContext context, Type destinationType, PreviewState previewState);

        /// <summary>
        /// Gets a query for nodes which map to <typeparamref name="TDestination"/>.
        /// </summary>
        /// <typeparam name="TDestination">The type to map to.</typeparam>
        /// <returns>A fluent configuration for the query.</returns>
        IContentQuery<TDestination> Query<TDestination>()
            where TDestination : class, new();


        IDictionary<string, string> Shortcodes { get; set; }
    }
}
