﻿namespace HighlyVisible.UPoco.Mapper
{
    public struct PreviewState
    {
        public bool IsInPreviewMode { get; set; }
        public bool IsGlobalPreview { get; set; }
        public int OriginatingContentId { get; set; }
    }
}